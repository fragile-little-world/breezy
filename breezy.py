import user_config

if user_config.TESTING:

    from interfaces.weather.dummy import DummyInterface as WeatherInterface
    from interfaces.timing.dummy import DummyInterface as TimingInterface
    from interfaces.notification.console import ConsoleInterface as NotificationInterface

    lo_temp = 66  # Degrees Fahrenheit
    hi_temp = 78  # Degrees Fahrenheit

else:
    if user_config.DARK_API is not None:
        from interfaces.weather.darksky import DarkSkyInterface as WeatherInterface

    if user_config.GMAIL_PWD is not None and user_config.GMAIL_USR is not None:
        from interfaces.notification.gmail import GmailInterface as NotificationInterface
    else:
        from interfaces.notification.console import ConsoleInterface as NotificationInterface

    from interfaces.timing.stdlib import PythonTimeInterface as TimingInterface

    hi_temp = user_config.HI_TEMP
    lo_temp = user_config.LO_TEMP

msg_address = user_config.MSG_ADDRESS

weather = WeatherInterface(user_config)
notifier = NotificationInterface(user_config)
timing = TimingInterface()

prev_temp = None

latitude = 30.682948
longitude = -88.018285

while True:

    current_temp = weather.current_temp(latitude, longitude)
    hourly_temp = weather.hourly_temps(latitude, longitude)
    wait_hours = None

    if current_temp is None or hourly_temp is None:
        raise IOError("Weather interface failed to return weather!")

    if hi_temp > current_temp > lo_temp:
        notifier.send_msg("Open your window for a temperate breeze",
                          "Weather Update", msg_address)

        prev_temp = current_temp

        temp_idx = 0
        for temp in hourly_temp:
            if temp > hi_temp or temp < lo_temp:
                break
            else:
                temp_idx += 1

        wait_hours = temp_idx + 1

    elif prev_temp is not None:
        if prev_temp > current_temp:
            notifier.send_msg("Close your window, it's getting cold out",
                              "Weather Update", msg_address)
        elif prev_temp < current_temp:
            notifier.send_msg("Close your window, it's getting hot out",
                              "Weather Update", msg_address)
        else:
            notifier.send_msg("Close your window to keep out the weather",
                              "Weather Update", msg_address)
        temp_idx = 0
        for temp in hourly_temp:
            if hi_temp > temp > lo_temp:
                break
            else:
                temp_idx += 1

        wait_hours = temp_idx + 1

        prev_temp = current_temp

    else:

        prev_temp = current_temp

        temp_idx = 0
        for temp in hourly_temp:
            if hi_temp > temp > lo_temp:
                break
            else:
                temp_idx += 1

        wait_hours = temp_idx + 1

    timing.wait_time(hours=wait_hours)
