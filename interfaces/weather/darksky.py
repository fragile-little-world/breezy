from interfaces.weather.general import WeatherInterface
from darksky.api import DarkSky
from darksky.types import languages, units, weather
from datetime import datetime as dt


class DarkSkyInterface(WeatherInterface):

    def __init__(self, config):
        super().__init__(config.DARK_API)
        self.darksky = DarkSky(self.api_key)

    def refresh(self, latitude, longitude):
        last_weather = self.cached_weather.get((latitude, longitude))

        if last_weather is None or (last_weather[0] - dt.utcnow()).seconds > 60:
            new_weather = self.darksky.get_forecast(
                latitude, longitude,
                extend=True,
                lang=languages.ENGLISH,
                values_units=units.AUTO,
                timezone="UTC"
            )

            self.cached_weather[(latitude, longitude)] = [dt.now(), new_weather]

    def current_temp(self, latitude, longitude):
        self.refresh(latitude, longitude)

        current_weather = self.cached_weather.get((latitude, longitude))[1]

        return current_weather.currently.temperature

    def hourly_temps(self, latitude, longitude):
        self.refresh(latitude, longitude)

        current_weather = self.cached_weather.get((latitude, longitude))[1]

        return [hour.temperature for hour in current_weather.hourly.data]
