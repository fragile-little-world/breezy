from abc import ABC, abstractmethod


class WeatherInterface(ABC):

    def __init__(self, api_key=None):
        self.api_key = api_key
        self.cached_weather = dict()
        super().__init__()

    @abstractmethod
    def refresh(self, latitude, longitude):
        pass

    @abstractmethod
    def current_temp(self, latitude, longitude):
        pass

    @abstractmethod
    def hourly_temps(self, latitude, longitude):
        pass
