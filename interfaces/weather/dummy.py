from interfaces.weather.general import WeatherInterface


class DummyInterface(WeatherInterface):

    def __init__(self, config):
        super().__init__(None)
        self.current = [65, 75, 80, 72, 65]
        self.hourly = [[64, 62], [74, 73, 72, 67, 65], [82, 85, 83, 79], [72, 73, 74, 75, 79],[65, 66, 67, 71]]

    def refresh(self, latitude, longitude):
        pass

    def current_temp(self, latitude, longitude):
        try:
            return self.current.pop()
        except IndexError:
            return None


    def hourly_temps(self, latitude, longitude):
        try:
            return self.hourly.pop()
        except IndexError:
            return None