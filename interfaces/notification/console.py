from interfaces.notification.general import NotificationInterface


class ConsoleInterface(NotificationInterface):

    def __init__(self, config):
        super().__init__(None)

    def send_msg(self, body, subject, address):
        super().send_msg(body, subject, address)
