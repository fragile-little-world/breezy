from abc import ABC, abstractmethod


class NotificationInterface(ABC):

    def __init__(self, api_key=None):
        self.api_key = api_key
        super().__init__()

    @abstractmethod
    def send_msg(self, body, subject, address):
        print(f"To: {address}\nSubject: {subject}\n\n{body}\n")
