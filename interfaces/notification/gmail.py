from interfaces.notification.general import NotificationInterface
import smtplib
from email.message import EmailMessage


class GmailInterface(NotificationInterface):

    def __init__(self, config):
        super().__init__(config.GMAIL_PWD)
        self.sender_name = config.GMAIL_USR

        try:
            self.server = smtplib.SMTP_SSL("smtp.gmail.com:465")
            self.server.ehlo()
            self.server.login(self.sender_name, self.api_key)
        except Exception as e:
            self.server.close()
            raise e

        if config.TESTING:
            debug_level = 2
        else:
            debug_level = 0

        self.server.set_debuglevel(debug_level)

    def __del__(self):
        self.server.close()

    def send_msg(self, body, subject, address):
        msg = EmailMessage()
        msg.set_content(body)
        msg["Subject"] = subject
        msg["From"] = self.sender_name
        msg["To"] = address

        self.server.send_message(msg)
