from interfaces.timing.general import TimingInterface


class DummyInterface(TimingInterface):

    def __init__(self):
        super().__init__()

    def wait_until(self, utc_time):
        super.wait_until(utc_time)

    def wait_time(self, weeks=0, days=0, hours=0, minutes=0, seconds=0):
        print(f"Waiting {weeks} weeks, {days} days, {hours} hours, {minutes} minutes, and {seconds} seconds.\n")
