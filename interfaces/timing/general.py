from abc import ABC, abstractmethod


class TimingInterface(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def wait_until(self, utc_time):
        pass

    @abstractmethod
    def wait_time(self, weeks=0, days=0, hours=0, minutes=0, seconds=0):
        pass
