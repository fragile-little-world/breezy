from interfaces.timing.general import TimingInterface
from datetime import datetime as dt
from time import sleep
from datetime import timedelta


class PythonTimeInterface(TimingInterface):

    def __init__(self):
        super().__init__()

    def wait_until(self, utc_time):

        while dt.utcnow() < utc_time:
            sleep(5)

    def wait_time(self, weeks=0, days=0, hours=0, minutes=0, seconds=0):
        difference = timedelta(weeks=weeks, days=days, hours=hours, seconds=seconds)

        self.wait_until(dt.utcnow() + difference)


